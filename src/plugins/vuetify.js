// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'
import { VDataTable } from 'vuetify/labs/VDataTable'
import es from "vuetify/lib/locale/es"

const customTheme = {
  dark: false,
  colors: {
    background: "#15202b",
    surface: "#15202b",
    primary: "#3f51b5",
    secondary: "#00bcd4",
    accent: "#607d8b",
    error: "#e91e63",
    warning: "#ff5722",
    info: "#009688",
    success: "#4caf50",
  },
}

export default createVuetify({

    theme: {
      defaultTheme: 'customTheme',
      themes: {
        customTheme,
      }
    },
    lang: {
      locales: { es },
        current: "es",
    },
    components: {
      VDataTable,
    },
})
