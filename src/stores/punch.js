import { defineStore } from "pinia";

const URL_API = 'https://keyence-backend-ww85.onrender.com'

export const usePunchStore = defineStore("punchStore", {
  state: () => ({
    punchsData: [],
    punchsTemplate: [],
    userData: null,
    loadingUser: false,
    loadingSession: false,
    error: false,
    errorMsg: '',
  }),
  actions: {
    async getAllPunch() {

      this.loadingUser = true;
      try {
        const response = await fetch(`${URL_API}/punch`);
        const punchs = await response.json();
        this.punchsData = punchs;

      } catch (error) {
        console.log(error);
      } finally {
        this.loadingUser = false;
      }
    },
    async savePunch(item) {
      this.loadingUser = true;
      try {
        const method = item?.id ? 'PUT' : 'POST'
        const update = await fetch(`${URL_API}/punch`, {
          method: method,
          body: JSON.stringify(item),
          headers: { "Content-type": "application/json; charset=UTF-8" }
        })
        await update.json().then((response) => {
          this.checkErrors(response)
        });


      } catch (error) {
        this.error = true;
        this.errorMsg = error.message;
      } finally {
        this.loadingUser = false;
      }
    },
    async deletePunch(item) {
      this.loadingUser = true;
      try {
        const deletePunch = await fetch(`${URL_API}/punch`, {
          method: "DELETE",
          body: JSON.stringify({
            id: item
          }),
          headers: { "Content-type": "application/json; charset=UTF-8" }
        })
        await deletePunch.json().then((response) => {
          this.checkErrors(response)
        });

      } catch (error) {
        this.error = true;
        this.errorMsg = error.message;
      } finally {
        this.loadingUser = false;
      }
    },
    async uploadTemplates(files){
      this.loadingUser = true;
      try {
        const formData = new FormData();
        formData.append("file", files[0])
        const update = await fetch(`${URL_API}/punch/uploadPunchTemplate`, {
          method: 'POST',
          body: formData,
        })
        await update.json().then((response) => {
          this.checkErrors(response)
        });


      } catch (error) {
        this.error = true;
        this.errorMsg = error.message;
      } finally {
        this.loadingUser = false;
      }
    },
    checkErrors(response) {
      try {
        if (response?.status !== 200 && response?.status !== 201) {
          //Verificamos errores
          this.error = true;
          if (response?.errors) {
            let errorTmp = '';
            response.errors.forEach((data) => {
              errorTmp += `<li>${data.msg}</li>`
            })
            this.errorMsg = `<ul>${errorTmp}</ul>`
          } else {
            this.errorMsg = response.message;
          }
          throw new Error(`Ocurrió un error: ${this.errorMsg}`);
        }

        this.error = false;

      } catch (error) {
        console.log(error.message)
      }
    }
    /*async registerUser(email, password) {
        this.loadingUser = true;
        try {
            const { user } = await createUserWithEmailAndPassword(
                auth,
                email,
                password
            );
            this.userData = { email: user.email, uid: user.uid };
            router.push("/");
        } catch (error) {
            console.log(error);
        } finally {
            this.loadingUser = false;
        }
    },
    async loginUser(email, password) {
        this.loadingUser = true;
        try {
            const { user } = await signInWithEmailAndPassword(
                auth,
                email,
                password
            );
            this.userData = { email: user.email, uid: user.uid };
            router.push("/");
        } catch (error) {
            console.log(error);
        } finally {
            this.loadingUser = false;
        }
    },
    async logoutUser() {
        try {
            await signOut(auth);
            this.userData = null;
            router.push("/login");
        } catch (error) {
            console.log(error);
        }
    },
    currentUser() {
        return new Promise((resolve, reject) => {
            const unsuscribe = onAuthStateChanged(
                auth,
                (user) => {
                    if (user) {
                        this.userData = {
                            email: user.email,
                            uid: user.uid,
                        };
                    } else {
                        this.userData = null;
                    }
                    resolve(user);
                },
                (e) => reject(e)
            );
            unsuscribe();
        });
    },*/
  },
});